package bsa.java.concurrency.interceptor;

import lombok.extern.java.Log;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@Log
public class LogInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
	                         Object handler) throws Exception {
		var queryString = Objects.nonNull(request.getQueryString())
				? '?' + request.getQueryString()
				: "";

		var logMessage = "User request:: " +
				"Method: '" + request.getMethod() +
				"', URL: '" + request.getRequestURL() + queryString;
		log.info(logMessage);

		return super.preHandle(request, response, handler);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
	                            Object handler, Exception ex) throws Exception {
		log.info("Answer status:" + response.getStatus());

		super.afterCompletion(request, response, handler, ex);
	}

}
