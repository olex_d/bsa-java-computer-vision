package bsa.java.concurrency.formatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Service
public class PathFormatter {

	@Value("${server.current.address}")
	private String currentUrl;

	public String pathToUrl(Path path) {
		return currentUrl + path.toString().replace("\\", "/");
	}

	public Path linkToPath(String gifUrl) {
		var gifPath = gifUrl.replace(currentUrl, "");
		return Path.of(gifPath);
	}

}
