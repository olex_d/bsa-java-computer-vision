package bsa.java.concurrency.hash;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.concurrent.CompletableFuture;

@Service
public class DHasher {

	private final static byte HASH_SIZE = 9;

	@Async
	public CompletableFuture<Long> calculateHash(byte[] image) {
		try {
			var img = ImageIO.read(new ByteArrayInputStream(image));
			var preprocessedImage = preprocessImage(img);
			var imgHash = calculateDHash(preprocessedImage);
			return CompletableFuture.completedFuture(imgHash);
		} catch (Exception err) {
			throw new RuntimeException(err.getMessage());
		}
	}

	private static BufferedImage preprocessImage(BufferedImage image) {
		var result = image.getScaledInstance(HASH_SIZE, HASH_SIZE, Image.SCALE_SMOOTH);
		var output = new BufferedImage(HASH_SIZE, HASH_SIZE, BufferedImage.TYPE_BYTE_GRAY);
		output.getGraphics().drawImage(result, 0, 0, null);
		return output;
	}

	private static int brightnessScore(int rgb) {
		return rgb & 0b11111111;
	}

	public static long calculateDHash(BufferedImage processedImage) {
		long hash = 0;
		for (var row = 1; row < HASH_SIZE - 1; row++) {
			for (var col = 1; col < HASH_SIZE; col++) {
				var prev = brightnessScore(processedImage.getRGB(row - 1, col - 1));
				var current = brightnessScore(processedImage.getRGB(row, col));
				hash |= current > prev ? 1 : 0;
				hash <<= 1;
			}
		}
		return hash;
	}

}
