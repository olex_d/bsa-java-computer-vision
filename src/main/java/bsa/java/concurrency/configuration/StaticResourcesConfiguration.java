package bsa.java.concurrency.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;

@Configuration
public class StaticResourcesConfiguration implements WebMvcConfigurer {

	@Value("${fs.base}")
	private String sharableFolder;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		var baseFolderPath = Path.of(sharableFolder).toAbsolutePath();
		var sharedFolderPath = "file:///" + baseFolderPath + "\\";

		registry
				.addResourceHandler("/images/**")
				.addResourceLocations(sharedFolderPath);
	}

}
