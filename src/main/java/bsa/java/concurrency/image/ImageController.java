package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDto;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/image")
@Log
@Validated
public class ImageController {

	private final ImageService imageService;

	public ImageController(ImageService imageService) {
		this.imageService = imageService;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ConstraintViolationException.class})
	public String handleException(Exception ex) {
		log.severe(ex.getMessage());
		return ex.getMessage();
	}

	@PostMapping("/batch")
	@ResponseStatus(HttpStatus.CREATED)
	public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
		var images = Arrays.stream(files)  // convert multipart files into byte[] list
				.parallel()
				.map(file -> {
					try {
						return file.getBytes();
					} catch (IOException ex) {
						log.severe(ex.getMessage());
						return new byte[0];
					}
				})
				.collect(Collectors.toList());

		imageService.batchImages(images);
	}

	@PostMapping("/search")
	@ResponseStatus(HttpStatus.OK)
	public List<SearchResultDto> searchMatches(@RequestParam("image") MultipartFile file,
	                                           @RequestParam(value = "threshold", defaultValue = "0.9")
	                                           @DecimalMin("0.1") @DecimalMax("1") String threshold) {
		try {
			var image = file.getBytes();
			var match = imageService.searchMatches(image, Double.parseDouble(threshold));

			if (match.isEmpty()) imageService.batchImage(image);

			return match;
		} catch (IOException ex) {
			log.severe(ex.getMessage());
			return new ArrayList<>();
		}
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteImage(@PathVariable("id") UUID imageId) {
		imageService.deleteImage(imageId);
	}

	@DeleteMapping("/purge")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void purgeImages() {
		imageService.deleteImages();
	}

}
