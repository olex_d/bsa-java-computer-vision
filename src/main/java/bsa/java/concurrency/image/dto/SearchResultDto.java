package bsa.java.concurrency.image.dto;

import java.util.UUID;

public interface SearchResultDto {
	UUID getImageId();
	String getImageUrl();
	Double getMatchPercent();
}
