package bsa.java.concurrency.image;

import bsa.java.concurrency.fs.FileSystem;
import bsa.java.concurrency.hash.DHasher;
import bsa.java.concurrency.image.dto.SearchResultDto;
import lombok.extern.java.Log;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Service
@Log
public class ImageService {

	private final ImageRepository imageRepository;
	private final DHasher dHasher;
	private final FileSystem fileSystem;

	public ImageService(ImageRepository imageRepository, DHasher dHasher, FileSystem fileSystem) {
		this.imageRepository = imageRepository;
		this.dHasher = dHasher;
		this.fileSystem = fileSystem;
	}

	public void batchImages(List<byte[]> images) {
		images.parallelStream().forEach(this::batchImage);
	}

	@Async
	public void batchImage(byte[] image) {
		try {
			var imgHash = dHasher.calculateHash(image);
			var imgLink = fileSystem.saveImageToSharable(image);

			var newImageRecord = Image.builder()
					.hash(imgHash.get())
					.link(imgLink.get())
					.build();
			newImageRecord = imageRepository.save(newImageRecord);

			log.info("New image added" + newImageRecord);
		} catch (ExecutionException | InterruptedException | IOException ex) {
			log.severe(ex.getMessage());
		}
	}

	public List<SearchResultDto> searchMatches(byte[] image, double threshold) {
		try {
			var imgHash = dHasher.calculateHash(image);
			var results = imageRepository.searchMatches(imgHash.get(), threshold);
			return results.get();
		} catch (InterruptedException | ExecutionException ex) {
			log.severe(ex.getMessage());
			return new ArrayList<>();
		}
	}

	@Async
	public void deleteImage(UUID imageId) {
		var dbImage = imageRepository.findById(imageId);
		if (dbImage.isEmpty()) throw new EntityNotFoundException("Image with ID: " + imageId + " hasn't been found");

		var imageLink = dbImage.get().getLink();
		imageRepository.delete(dbImage.get());
		fileSystem.deleteImageByLink(imageLink);

		log.info("Image with ID: " + imageId + " deleted");
	}

	@Async
	public void deleteImages() {
		imageRepository.deleteAll();
		fileSystem.deleteAllImages();
		log.info("All images have been deleted");
	}

}
