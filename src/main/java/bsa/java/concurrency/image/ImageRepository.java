package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface ImageRepository extends JpaRepository<Image, UUID> {

	@Async
	@Query(value =
			"SELECT DISTINCT ON (cI.matchPercent) " +
					"cI.imageId::::varchar, cI.imageUrl, cI.matchPercent " +
			"FROM ( " +
				"SELECT i.id imageId, i.link imageUrl, " +
				"(1 - ((SELECT COUNT(*) FROM regexp_matches(((i.hash # ?1)::::bit(64)::::varchar), '1', 'g'))::::double precision / 64)) * 100 matchPercent " +
				"FROM images i " +
			" ) as cI " +
			"WHERE cI.matchPercent >= ?2 * 100 " +
			"ORDER BY cI.matchPercent DESC ",
			nativeQuery = true)
	CompletableFuture<List<SearchResultDto>> searchMatches(long imgHash, double threshold);
}
