package bsa.java.concurrency.fs;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
@Log
public class FileSystemSeeder {

	@Value("${fs.base}")
	private String rootDir;

	@EventListener
	public void seed(ContextRefreshedEvent event) throws IOException {
		var rootPath = Path.of(rootDir);
		if (!Files.exists(rootPath)) {
			var createdDir = Files.createDirectories(rootPath);
			log.info(createdDir.toAbsolutePath().toString() + " directory has been seeded");
		}
	}

}
