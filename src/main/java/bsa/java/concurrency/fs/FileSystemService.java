package bsa.java.concurrency.fs;

import bsa.java.concurrency.formatter.PathFormatter;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
@Log
public class FileSystemService implements FileSystem {

	@Value("${fs.base}")
	private String rootDir;

	private final PathFormatter pathFormatter;

	public FileSystemService(PathFormatter pathFormatter) {
		this.pathFormatter = pathFormatter;
	}

	@Async
	public CompletableFuture<String> saveImageToSharable(byte[] imageBytes) throws IOException {
		var rootPath = Path.of(rootDir);
		if (!Files.exists(rootPath)) Files.createDirectories(rootPath);

		var newName = UUID.randomUUID().toString() + ".jpg";
		var newImgPath = rootPath.resolve(newName);
		Files.createFile(newImgPath);

		try (var outputStream = new FileOutputStream(newImgPath.toString())) {
			outputStream.write(imageBytes);
		}

		var newImgLink = pathFormatter.pathToUrl(newImgPath);
		return CompletableFuture.completedFuture(newImgLink);
	}

	@Async
	public void deleteImageByLink(String imageLink) {
		try {
			var imagePath = pathFormatter.linkToPath(imageLink);
			Files.deleteIfExists(imagePath);
		} catch (IOException ex) {
			log.severe(ex.getMessage());
		}
	}

	@Async
	public void deleteAllImages() {
		try {
			Files.walk(Path.of(rootDir))
					.sorted(Comparator.reverseOrder())
					.map(Path::toFile)
					.filter(file -> !file.toPath().equals(Path.of(rootDir)))
					.forEach(File::delete);
		} catch (IOException ex) {
			log.severe(ex.getMessage());
		}
	}
}
