package bsa.java.concurrency.fs;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public interface FileSystem {
	CompletableFuture<String> saveImageToSharable(byte[] imageBytes) throws IOException;

	void deleteImageByLink(String imageLink);

	void deleteAllImages();
}
